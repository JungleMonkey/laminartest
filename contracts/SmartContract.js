const keccak256 = require('keccak256')  // Cryptographically secure hashing engine used by Ethereum
const Prando = require('prando')        // deterministic random number generator

function transfer(amount, to) {
    console.log('Transfer', amount, 'to', to)
}

class SmartContract {
    constructor(deposit, accounts) {
        'use strict'
        this.maxGuess = 10000
        this.deposit = deposit
        this.accounts = accounts
        this.hashedNumbers = {}
        this.randomValues = {}
        this.isFinished = false
        this.isDistributing = false
        this.results = {}           // Used for test analysis

        if (typeof(deposit) !== 'number' || deposit <= 0) {
            throw new Error('Deposit needs to be a number greater than 0')
        }

        if (this.accounts.length < 2) {
            throw new Error('Needs at least two participants')
        }

    }

    submitHashedNumber(account, hashedNumber) {
        'use strict'
        // Prevent the updating of values once participants start to reveal their numbers
        if (this.isFinished) {
            throw new Error('New entries are no longer permitted.')
        }

        if (!this.accounts.includes(account)) {
            console.log('Invalid participant = ', account)
            throw new Error(`Invalid participant "${account}"`)
        }

        this.hashedNumbers[account] = hashedNumber
    }


    // May be called by anyone, but not until all hashed numbers have been committed
    finishEntries() {
        'use strict'
        if (this.isFinished) {
            throw new Error('Entry stage has completed')
        }
        if (Object.keys(this.hashedNumbers).length !== this.accounts.length) {
            throw new Error('Some participants have yet to submit their hashed number')
        }

        this.isFinished = true
    }


    // May only be called by valid participants, but not until a call to finish has succeeded
    revealRandomNumber(account, secret, number) {
        if (!this.isFinished) {
            throw new Error('You may not reveal your number until entries have finished')
        }
        if (this.isDistributing) {
            throw new Error('Results are being distributed, or have already been distributed')
        }
        if (!this.accounts.includes(account)) {
            throw new Error(`Invalid participant "${account}"`)
        }
        if (typeof(number) !== 'number') {
            throw new Error('Number is not a number')
        } else if (!Number.isInteger(number)) {
            throw new Error('Number must be an integer')
        } else if (number < 0 || number > this.maxGuess) {
            throw new Error(`Number must be between 1 and ${this.maxGuess} inclusive`)
        }
        let hashedNumber = this.hashedNumbers[account]

        let hash = keccak256(String(secret) + account + String(number)).toString('hex')

        if (hash !== hashedNumber) {
            throw new Error('Hashed input does not match')
        }

        this.randomValues[account] = number
    }

    
    // Maybe be called by anyone, but not until all entries have finished revealing their numbers
    distributePrizes() {
        'use strict'
        if (!this.isFinished) {
            throw new Error('Entries are not finished yet')
        }

        if (this.isDistributing) {
            throw new Error('Results are being distributed, or have already been distributed')
        }
        
        if (Object.keys(this.hashedNumbers).length !== Object.keys(this.randomValues).length) {
            throw new Error('Some participants have not revealed their random number yet')
        }

        this.isDistributing = true

        const values = Object.values(this.randomValues)

        //let combined = now()    // Don't do this.  Makes it non-deterministic
        
        // Deterministic starting point so as can be reproduced on any blockchain node
        let combined = 42  // Seed with Monty Pythons answer to the meaning of life.
        for (const value of values) {
            combined ^= value
        }
        
        // Seed the random number generator with a reproducable seed that is unique to the inputs
        this.rng = new Prando(combined);

        /*
            Calculate random prize amounts before distributing.
            This ensures all participants have equal chance to the prize amounts.
            Note: this method will not work well for lots of participants as the
            deposit will be used up fairly quickly.  Perhaps use another form of distribution.
        */
        let prizes = []
        let remain = this.deposit
        for (let i = 0; i < this.accounts.length -1; i++) {
            const prize = Math.floor(remain * this.rng.next())
            prizes.push(prize)

            remain -= prize
        }
        prizes.push(remain)


        /*
            Use a linear distribution to more evenly distribute the prizes over the range

            Note: May fail the requirement that the amounts received are to appear random
        */
        // let prizes = []
        // let remain = this.deposit
        // let gauss = ((this.accounts.length)/2) * (this.accounts.length + 1)  // Carl Gauss Formula
        // for (let i = 0; i < this.accounts.length -1; i++) {
        //     const prize = Math.floor((i + 1) / gauss * this.deposit)
        //     prizes.push(prize)
        //     remain -= prize
        // }
        // prizes.push(remain)
        // console.log('prizes = ', prizes)


        // Randomly reorder the prizes array using the Durstenfeld shuffle algorithm
        for (let i = prizes.length - 1; i > 0; i--) {
            let j = Math.floor(this.rng.next() * (i + 1));
            let tmp = prizes[i];
            prizes[i] = prizes[j];
            prizes[j] = tmp;
        }

       
        // Distribute the prizes to the participants
        for (let i = 0; i < this.accounts.length; i++) {
            const account = this.accounts[i]
            //const toSend = randomPrizes[i]
            const toSend = prizes[i]
            if (toSend > 0) {
                transfer(toSend, account)
            }

            this.results[account] = toSend; // Purely for testing
        }
    }
}

module.exports = SmartContract;