const path = require('path')
const keccak256 = require('keccak256')  // Cryptographically secure hashing engine used by Ethereum
const tools = require('./tools')

const smartContract = require('./contracts/SmartContract')

global.appRoot = path.resolve(__dirname);
global.tempFileDir = path.join(global.appRoot,'tmp');

let maxGuess = 10000

let secrets = []

let participantA = {}
let participantB = {}
let participantC = {}

function main() {
    let args = process.argv.slice(2);

    // For testing, load a random phrase from file
    secrets = tools.loadObjectFromFile(path.join(global.appRoot, 'secrets.json'))

    // Common to most tests
    let value = Math.floor(Math.random() * maxGuess)
    let secret = secrets[Math.floor(Math.random() * secrets.length)]
    participantA = {
        account:        'A',
        secret:         secret,
        value:          value,
        hash:           keccak256(secret + 'A' + value.toString()).toString('hex')
    }

    value = Math.floor(Math.random() * maxGuess)
    secret = secrets[Math.floor(Math.random() * secrets.length)]
    participantB = {
        account:        'B',
        secret:         secret,
        value:          value,
        hash:           keccak256(secret + 'B' + value.toString()).toString('hex')
    }
    
    value = Math.floor(Math.random() * maxGuess)
    secret = secrets[Math.floor(Math.random() * secrets.length)]
    participantC = {
        account:        'C',
        secret:         secret,
        value:          value,
        hash:           keccak256(secret + 'C' + value.toString()).toString('hex')
    }

    // This participant is to be used as an unknown participant and not included
    // during the initialisation stage
    value = Math.floor(Math.random() * maxGuess)
    secret = secrets[Math.floor(Math.random() * secrets.length)]
    participantD = {
        account:        'D',
        secret:         secret,
        value:          value,
        hash:           keccak256(secret + 'D' + value.toString()).toString('hex')
    }

    if (args.length > 0) {
        switch (args[0]) {
            case '1':
                normalRun()
                break
            case '2':
                deterministic()
                break
            case '3':
                outOfSequence()
                break
            case '4':
                invalidInputs()
                break
            case '5':
                fairness()
                break
            default:
                console.log(`Unsupported argument ${args[0]}`)
                showArguments()
                break
        }
    } else {
        showArguments()
    }
}

function showArguments() {
    console.log('Please specify an argument.')
    console.log('    1 - Normal run.')
    console.log('    2 - Test if deterministic')
    console.log('    3 - Out of sequence')
    console.log('    4 - Invalid inputs')
    console.log('    5 - Fairness')
}

// 1
function normalRun() {
    const deposit = 1000
    const participants = [participantA.account, participantB.account, participantC.account]
    const runHistory = []
    const testInputs = []
    let testResults = []

    const contract = new smartContract(deposit, participants)

    let commands = []
    commands.push({ command: 'contract.submitHashedNumber(participantA.account, participantA.hash)'                          , expected: 'OK' })
    commands.push({ command: 'contract.submitHashedNumber(participantB.account, participantB.hash)'                          , expected: 'OK' })
    commands.push({ command: 'contract.submitHashedNumber(participantC.account, participantC.hash)'                          , expected: 'OK' })
    commands.push({ command: 'contract.finishEntries()'                                                                      , expected: 'OK' })
    commands.push({ command: 'contract.revealRandomNumber(participantA.account, participantA.secret, participantA.value)'    , expected: 'OK' })
    commands.push({ command: 'contract.revealRandomNumber(participantB.account, participantB.secret, participantB.value)'    , expected: 'OK' })
    commands.push({ command: 'contract.revealRandomNumber(participantC.account, participantC.secret, participantC.value)'    , expected: 'OK' })
    commands.push({ command: 'contract.distributePrizes()'                                                                   , expected: 'OK' })

    for (let i = 0; i < commands.length; i++) {
        const testResult = {
            command: commands[i].command,
            expected: commands[i].expected,
            result: null
        }

        try {
            eval(commands[i].command)
            testResult.result = 'OK'
        } catch(err) {
            if (err.message) {
                testResult.result = err.message
                console.log(err.message)
            } else {
                testResult.result = err
                console.log(err)
            }
        }
        
        runHistory.push(testResult)
    }

    testInputs.push(contract.randomValues)
    testResults.push(contract.results)

    let filename = 'test_normalRun_Inputs.csv'
    console.log(`Results saved to ${path.join(global.tempFileDir, filename)}`)
    tools.saveResultsToCSVFile(testInputs, filename)

    filename = 'test_normalRun_Run.csv'
    console.log(`Results saved to ${path.join(global.tempFileDir, filename)}`)
    tools.saveResultsToCSVFile(runHistory, filename)

    filename = 'test_normalRun_Results.csv'
    console.log(`Results saved to ${path.join(global.tempFileDir, filename)}`)
    tools.saveResultsToCSVFile(testResults, filename)
}   // 1 - normalRun


// 2
function deterministic() {
    const deposit = 1000
    const participants = [participantA.account, participantB.account, participantC.account]
    const runHistory = []
    const testInputs = []
    let testResults = []
    let iterations = 10

    // Run through multiple times with the same inputs to verify that it is in fact deterministic
    for (let i = 0; i < iterations; i++) {
        const contract = new smartContract(deposit, participants)

        let commands = []
        commands.push({ command: 'contract.submitHashedNumber(participantA.account, participantA.hash)'                         , expected: 'OK' })
        commands.push({ command: 'contract.submitHashedNumber(participantB.account, participantB.hash)'                         , expected: 'OK' })
        commands.push({ command: 'contract.submitHashedNumber(participantC.account, participantC.hash)'                         , expected: 'OK' })
        commands.push({ command: 'contract.finishEntries()'                                                                     , expected: 'OK' })
        commands.push({ command: 'contract.revealRandomNumber(participantA.account, participantA.secret, participantA.value)'   , expected: 'OK' })
        commands.push({ command: 'contract.revealRandomNumber(participantB.account, participantB.secret, participantB.value)'   , expected: 'OK' })
        commands.push({ command: 'contract.revealRandomNumber(participantC.account, participantC.secret, participantC.value)'   , expected: 'OK' })
        commands.push({ command: 'contract.distributePrizes()'                                                                  , expected: 'OK' })

        for (let i = 0; i < commands.length; i++) {
            const testResult = {
                command: commands[i].command,
                expected: commands[i].expected,
                result: null
            }
    
            try {
                eval(commands[i].command)
                testResult.result = 'OK'
            } catch(err) {
                if (err.message) {
                    testResult.result = err.message
                    console.log(err.message)
                } else {
                    testResult.result = err
                    console.log(err)
                }
            }
            
            runHistory.push(testResult)
        }

        testInputs.push(contract.randomValues)
        testResults.push(contract.results)
    }

    let filename = 'test_deterministic_Inputs.csv'
    console.log(`Results saved to ${path.join(global.tempFileDir, filename)}`)
    tools.saveResultsToCSVFile(testInputs, filename)

    filename = 'test_deterministic_Run.csv'
    console.log(`Results saved to ${path.join(global.tempFileDir, filename)}`)
    tools.saveResultsToCSVFile(runHistory, filename)

    filename = 'test_deterministic_Results.csv'
    console.log(`Results saved to ${path.join(global.tempFileDir, filename)}`)
    tools.saveResultsToCSVFile(testResults, filename)
}   // 2 - deterministic


// 3
function outOfSequence() {
    const deposit = 1000
    const participants = [participantA.account, participantB.account, participantC.account]
    const runHistory = []
    const testInputs = []
    let testResults = []

    const contract = new smartContract(deposit, participants)

    let commands = []
    commands.push({ command: 'contract.submitHashedNumber(participantA.account, participantA.hash)'                         , expected: 'OK' })
    commands.push({ command: 'contract.submitHashedNumber(participantB.account, participantB.hash)'                         , expected: 'OK' })
    commands.push({ command: 'contract.finishEntries()'                                                                     , expected: 'Too early' })                                                   // C has not yet committed
    commands.push({ command: 'contract.submitHashedNumber(participantC.account, participantC.hash)'                         , expected: 'OK' })
    commands.push({ command: 'contract.submitHashedNumber(participantB.account, participantB.hash)'                         , expected: 'Should be fine' })
    commands.push({ command: 'contract.revealRandomNumber(participantA.account, participantA.secret, participantA.value)'   , expected: 'Too early' })
    commands.push({ command: 'contract.distributePrizes()'                                                                  , expected: 'Too early' })
    commands.push({ command: 'contract.finishEntries()'                                                                     , expected: 'OK' })
    commands.push({ command: 'contract.submitHashedNumber(participantA.account, participantA.hash)'                         , expected: 'Too late' })
    commands.push({ command: 'contract.finishEntries()'                                                                     , expected: 'Already run' })
    commands.push({ command: 'contract.revealRandomNumber(participantA.account, participantA.secret, participantA.value)'   , expected: 'OK' })
    commands.push({ command: 'contract.revealRandomNumber(participantB.account, participantB.secret, participantB.value)'   , expected: 'OK' })
    commands.push({ command: 'contract.revealRandomNumber(participantC.account, participantC.secret, participantC.value)'   , expected: 'OK' })
    commands.push({ command: 'contract.revealRandomNumber(participantA.account, participantA.secret, participantA.value)'   , expected: 'Should be fine' })
    commands.push({ command: 'contract.distributePrizes()'                                                                  , expected: 'OK' })
    commands.push({ command: 'contract.submitHashedNumber(participantA.account, participantA.hash)'                         , expected: 'Too late' })
    commands.push({ command: 'contract.revealRandomNumber(participantA.account, participantA.secret, participantA.value)'   , expected: 'Too late' })
    commands.push({ command: 'contract.finishEntries()'                                                                     , expected: 'Already run' })
    commands.push({ command: 'contract.distributePrizes()'                                                                  , expected: 'Already run' })

    for (let i = 0; i < commands.length; i++) {
        const testResult = {
            command: commands[i].command,
            expected: commands[i].expected,
            result: null
        }

        try {
            eval(commands[i].command)
            testResult.result = 'OK'
        } catch(err) {
            if (err.message) {
                testResult.result = err.message
                console.log(err.message)
            } else {
                testResult.result = err
                console.log(err)
            }
        }
        
        runHistory.push(testResult)
    }

    testInputs.push(contract.randomValues)
    testResults.push(contract.results)

    let filename = 'test_outOfSequence_Inputs.csv'
    console.log(`Results saved to ${path.join(global.tempFileDir, filename)}`)
    tools.saveResultsToCSVFile(testInputs, filename)

    filename = 'test_outOfSequence_Run.csv'
    console.log(`Results saved to ${path.join(global.tempFileDir, filename)}`)
    tools.saveResultsToCSVFile(runHistory, filename)

    filename = 'test_outOfSequence_Results.csv'
    console.log(`Results saved to ${path.join(global.tempFileDir, filename)}`)
    tools.saveResultsToCSVFile(testResults, filename)
}   // 3 - outOfSequence


// 4
function invalidInputs() {
    const deposit = 1000
    const participants = [participantA.account, participantB.account, participantC.account]
    const runHistory = []
    const testInputs = []
    let testResults = []

    const contract = new smartContract(deposit, participants)

    let commands = []
    commands.push({ command: 'contract.submitHashedNumber(participantA.account, participantA.hash)'                             , expected: 'OK' })
    commands.push({ command: 'contract.submitHashedNumber(participantB.account, participantA.hash)'                             , expected: 'Ok now but fail later' })
    commands.push({ command: 'contract.submitHashedNumber(participantD.account, participantD.hash)'                             , expected: 'Invalid participant' })
    commands.push({ command: 'contract.submitHashedNumber(participantC.account, participantC.hash)'                             , expected: 'OK' })
    commands.push({ command: 'contract.finishEntries()'                                                                         , expected: 'OK' })
    commands.push({ command: 'contract.revealRandomNumber(participantA.account, participantA.secret, participantA.value)'       , expected: 'OK' })
    commands.push({ command: 'contract.revealRandomNumber(participantB.account, participantB.secret, participantA.value)'       , expected: 'Not their hash' })
    commands.push({ command: 'contract.revealRandomNumber(participantB.account, participantB.secret, participantB.value)'       , expected: 'Still fail as doesn\'t match submitted value' })
    commands.push({ command: 'contract.revealRandomNumber(participantC.account, participantC.secret, participantC.value + 1)'   , expected: 'Value doesn\'t match' })
    commands.push({ command: 'contract.revealRandomNumber(participantC.account, participantC.secret, participantA.value)'       , expected: 'Value doesn\'t match' })
    commands.push({ command: 'contract.revealRandomNumber(participantC.account, participantC.secret, participantC.value)'       , expected: 'OK' })
    commands.push({ command: 'contract.revealRandomNumber(participantD.account, participantD.secret, participantD.value)'       , expected: 'Invalid participant' })
    commands.push({ command: 'contract.distributePrizes()'                                                                      , expected: 'Will always fail since B has corrupted the inputs' })

    for (let i = 0; i < commands.length; i++) {
        const testResult = {
            command: commands[i].command,
            expected: commands[i].expected,
            result: null
        }

        try {
            eval(commands[i].command)
            testResult.result = 'OK'
        } catch(err) {
            if (err.message) {
                testResult.result = err.message
                console.log(err.message)
            } else {
                testResult.result = err
                console.log(err)
            }
        }
        
        runHistory.push(testResult)
    }

    testInputs.push(contract.randomValues)
    testResults.push(contract.results)

    let filename = 'test_invalidInputs_Inputs.csv'
    console.log(`Results saved to ${path.join(global.tempFileDir, filename)}`)
    tools.saveResultsToCSVFile(testInputs, filename)

    filename = 'test_invalidInputs_Run.csv'
    console.log(`Results saved to ${path.join(global.tempFileDir, filename)}`)
    tools.saveResultsToCSVFile(runHistory, filename)

    filename = 'test_invalidInputs_Results.csv'
    console.log(`Results saved to ${path.join(global.tempFileDir, filename)}`)
    tools.saveResultsToCSVFile(testResults, filename)
}   // 4 - invalidInputs


// 5
function fairness() {
    const identities = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    const accounts = {}
    const testInputs = []
    const testResults = []

    let deposit = 1000
    let iterations = 10000
    let participants = 5

    if (participants > identities.length) {
        throw new Error(`You may not specify more than ${identities.length} participants`)
    }

    for (let iteration = 0; iteration < iterations; iteration++) {
        
        // Create the accounts
        for (let i = 0; i < participants; i++) {
            const account = identities.substr(i, 1)
            const value = Math.floor(Math.random() * maxGuess)
            const secret = secrets[Math.floor(Math.random() * secrets.length)]

            /*
                To prevent the use of rainbow tables to easily discover the values,
                hash the secret in conjunction with the account and value.
                Also ensures that the user can't choose a different value when
                executing revealRandomNumber
            */
            accounts[account] = {
                secret: secret,
                value: value,
                hash: keccak256(secret + account + value.toString()).toString('hex')
            }
        }

        const contract = new smartContract(deposit, Object.keys(accounts))

        // submit all entries
        for (const account in accounts) {
            contract.submitHashedNumber(account, accounts[account].hash)
        }

        // Anyone can send the finishEntries message to the smart contract
        contract.finishEntries()

        for (const account in accounts) {
            contract.revealRandomNumber(account, accounts[account].secret, accounts[account].value)
        }

        // Anyone can send the distributePrizes message to the smart contract indicating that it can start distributing the funds
        contract.distributePrizes()

        testInputs.push(contract.randomValues)
        testResults.push(contract.results)
    }

    let filename = 'test_fairness_Inputs.csv'
    console.log(`Results saved to ${path.join(global.tempFileDir, filename)}`)
    tools.saveResultsToCSVFile(testInputs, filename)

    filename = 'test_fairness_Results.csv'
    console.log(`Results saved to ${path.join(global.tempFileDir, filename)}`)
    tools.saveResultsToCSVFile(testResults, filename)
}   // 5 - fairness


main()