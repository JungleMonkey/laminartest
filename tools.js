const fs = require('fs');
const path = require('path')

module.exports = {
    loadObjectFromFile(filename) {
        'use strict';
        let result = null;
        let rawdata = null;
        try {
            rawdata = fs.readFileSync(filename);
            try {
                result = JSON.parse(rawdata);   // Works with numbers as well
            } catch(err) {
                result = rawdata.toString();
            }
            
        } catch (err) {
            console.log(err)
        }
        return result;
    },

    saveResultsToCSVFile: function(results, filename) {

        if (!fs.existsSync(global.tempFileDir)) {
            fs.mkdirSync(global.tempFileDir);
        }

        const writer = fs.createWriteStream(path.join(global.tempFileDir, filename))


        for (let i = 0; i < results.length; i++) {
            let textLine = ''
            if (i === 0) {
                for (const identifier in results[i]) {
                    if (textLine.length > 0) {
                        textLine += ','
                    }
                    textLine += identifier
                }
                writer.write(textLine + '\r\n')
                textLine = ''
            }

            for (const identifier in results[i]) {
                let quotes = ''
                if (String(results[i][identifier]).includes(',')) {
                    quotes = '"'
                }
                if (textLine.length > 0) {
                    textLine += ','
                }
                textLine += quotes + String(results[i][identifier]) + quotes
            }

            writer.write(textLine + '\r\n')
        }

        writer.end();
    },

    saveObjectToJsonFile(obj, filename) {
        'use strict';
        let outputString = '';
        try {
            //console.log('### Object Type: ' + typeof obj);

            switch (typeof obj) {
                case 'object': {
                    outputString = JSON.stringify(obj, null, 3);
                    break;
                }
                default: {
                    outputString = obj;
                    break;
                }
            }
            fs.writeFileSync(filename, outputString);
        } catch (err) {
            console.log(err)
        }
    }
}